FROM alpine

ARG VERSION=0.14.48

RUN adduser -h /var/syncthing -s /sbin/nologin -D syncthing && \
  apk add --no-cache --virtual .deps gnupg && \
  apk add --no-cache ca-certificates curl su-exec && \
  gpg --keyserver keyserver.ubuntu.com --recv-key D26E6ED000654A3E && \
  set -x && \
  mkdir /syncthing && \
  cd /syncthing && \
  curl -sLO https://github.com/syncthing/syncthing/releases/download/v${VERSION}/syncthing-linux-amd64-v${VERSION}.tar.gz && \
  curl -sLO https://github.com/syncthing/syncthing/releases/download/v${VERSION}/sha256sum.txt.asc && \
  gpg --verify sha256sum.txt.asc && \
  grep syncthing-linux-amd64 sha256sum.txt.asc | sha256sum && \
  tar -zxf syncthing-linux-amd64-v${VERSION}.tar.gz && \
  mv syncthing-linux-amd64-v${VERSION}/syncthing . && \
  rm -rf syncthing-linux-amd64-v${VERSION} sha256sum.txt.asc syncthing-linux-amd64-v${VERSION}.tar.gz && \
  apk del .deps

ENV STNOUPGRADE=1

HEALTHCHECK --interval=1m --timeout=10s CMD curl -s --fail localhost:8384 >/dev/null || exit 1

EXPOSE 8384/tcp 22000/tcp 21027/udp
COPY entrypoint /entrypoint

ENTRYPOINT ["/entrypoint"]